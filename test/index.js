/* global describe, it, before */

const chai = require('chai')
const chaiHttp = require('chai-http')
const should = chai.should()
const {app, Database} = require('index')
const DataStore = require('./datastore')
chai.use(chaiHttp)

describe('API.v1 Tests', function () {
  this.timeout(60 * 1000)
  before(done => {
    app.on('app_ready', () => {
      done()
    })
  })

  it('Should have an ok appstatus', done => {
    chai.request(app)
      .get('/v1/appstatus')
      .end((err, res) => {
        should.not.exist(err)
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').eql(true)
        res.body.should.have.property('data').eql('ok')
        done()
      })
  })

  require('./tests/users')

  require('./tests/auth')
})
