/* global describe, it */
const chai = require('chai')
const TestObjects = require('../objects')
const DataStore = require('../datastore')
const should = chai.should()
const {app} = require('index')

describe('Users', () => {
  describe('POST /v1/users', () => {
    it('Should create a valid new user', done => {
      chai.request(app)
        .post('/v1/users')
        .send(TestObjects.VALID_USER_ONE)
        .end((err, res) => {
          should.not.exist(err)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          res.body.should.have.property('data').and.be.a('object')
          res.body.should.have.property('token').and.be.a('string')
          res.body.data.should.have.property('email').eql(TestObjects.VALID_USER_ONE.email)
          res.body.data.should.have.property('username').eql(TestObjects.VALID_USER_ONE.username)
          res.body.data.should.have.property('created_at')
          res.body.data.should.have.property('updated_at')
          res.body.data.should.not.have.property('password')
          res.body.data.should.not.have.property('hash')
          DataStore.UserOneToken = res.body.token
          done()
        })
    })

    it('Should fail to create a duplicate user', done => {
      chai.request(app)
        .post('/v1/users')
        .send(TestObjects.VALID_USER_ONE)
        .end((err, res) => {
          should.exist(err)
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(false)
          res.body.should.have.property('code').eql('DuplicateEntryError')
          done()
        })
    })

    it('Should fail to create an invalid user', done => {
      chai.request(app)
        .post('/v1/users')
        .send(TestObjects.INVALID_USER)
        .end((err, res) => {
          should.exist(err)
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(false)
          res.body.should.have.property('code').eql('InputValidationError')
          done()
        })
    })

    it('Should create valid user two', done => {
      chai.request(app)
        .post('/v1/users')
        .send(TestObjects.VALID_USER_TWO)
        .end((err, res) => {
          should.not.exist(err)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          DataStore.UserTwoToken = res.body.token
          done()
        })
    })
  })

  describe('PUT /v1/users', () => {
    it('Should fail to update user without an auth token', done => {
      chai.request(app)
        .put('/v1/users')
        .send(TestObjects.USER_TWO_UPDATES)
        .end((err, res) => {
          should.exist(err)
          res.should.have.status(401)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(false)
          res.body.should.have.property('code').eql('InvalidAuthToken')
          res.body.should.not.have.property('errors')
          done()
        })
    })

    it('Should successfully update user two\'s user object', done => {
      chai.request(app)
        .put('/v1/users')
        .set('x-auth-token', DataStore.UserTwoToken)
        .send(TestObjects.USER_TWO_UPDATES)
        .end((err, res) => {
          should.not.exist(err)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          res.body.should.have.property('updated').eql(true)
          res.body.should.have.property('data').and.be.a('object')
          res.body.data.should.have.property('email').eql(TestObjects.USER_TWO_UPDATES.email)
          res.body.data.should.have.property('username').eql(TestObjects.VALID_USER_TWO.username)
          res.body.data.should.have.property('created_at')
          res.body.data.should.have.property('updated_at')
          res.body.data.should.not.have.property('password')
          res.body.data.should.not.have.property('hash')
          done()
        })
    })

    it('Should successfully update user two\'s password', done => {
      chai.request(app)
        .put('/v1/users/password')
        .set('x-auth-token', DataStore.UserTwoToken)
        .send(TestObjects.USER_TWO_PASSWORD)
        .end((err, res) => {
          should.not.exist(err)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          res.body.should.have.property('updated').eql(true)
          res.body.should.have.property('data').and.be.a('object')
          res.body.data.should.have.property('email').eql(TestObjects.USER_TWO_UPDATES.email)
          res.body.data.should.not.have.property('password')
          res.body.data.should.not.have.property('hash')
          done()
        })
    })
  })
})
