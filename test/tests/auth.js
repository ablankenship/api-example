/* global describe, it */
const chai = require('chai')
const TestObjects = require('../objects')
const DataStore = require('../datastore')
const should = chai.should()
const {app} = require('index')

describe('Auth', () => {
  describe('POST /v1/auth', () => {
    it('Should successfully authenticate user one', done => {
      chai.request(app)
        .post('/v1/auth')
        .send({
          email: TestObjects.USER_TWO_UPDATES.email,
          password: TestObjects.USER_TWO_PASSWORD.new_password
        })
        .end((err, res) => {
          should.not.exist(err)
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').eql(true)
          res.body.should.have.property('token').and.be.a('string')
          res.body.should.have.property('data').and.be.a('object')
          res.body.data.should.have.property('email').eql(TestObjects.USER_TWO_UPDATES.email)
          res.body.data.should.have.property('username').eql(TestObjects.VALID_USER_TWO.username)
          res.body.data.should.have.property('created_at')
          res.body.data.should.have.property('updated_at')
          res.body.data.should.not.have.property('password')
          res.body.data.should.not.have.property('hash')
          DataStore.UserTwoToken = res.body.token
          done()
        })
    })
  })
})
