module.exports = {
  VALID_USER_ONE: {
    email: 'test@test.com',
    password: 'ilikeapples1',
    username: 'bobdole'
  },
  VALID_USER_TWO: {
    email: 'test2@test2.com',
    password: 'applepie2',
    username: 'billbob'
  },
  USER_TWO_UPDATES: {
    email: 'test3@test3.com'
  },
  USER_TWO_PASSWORD: {
    old_password: 'applepie2',
    new_password: 'mmmpie33'
  },
  INVALID_USER: {
    email: 'asdkjfsajhf@ksdjfjasklfaj',
    password: 'applepie77',
    username: 'bobdole'
  }
}
