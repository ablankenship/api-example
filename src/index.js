if (!process.env.NODE_PATH) {
  throw new Error('NODE_PATH is not set, start the app using `yarn/npm start`')
}

const express = require('express')
const bodyParser = require('body-parser')
const config = require('config')
const response = require('libs/response')
const middleware = require('libs/middleware')
const {Database} = require('libs/db')
const http = require('http')
const app = express()
const server = http.createServer(app)
const router = express.Router()

router.use('/appstatus', middleware.appStatus)

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({extended: true}))
router.use(middleware.cors)
router.use('/auth', require('controllers/auth'))
router.use('/users', require('controllers/users'))

app.use(response.responses)
app.use('/v1', router)
app.use(response.errors)
app.use(response.catch404)

Database
  .authenticate()
  .then(ready)
  .catch(err => console.error('Unable to connect to the database:', err))

function ready () {
  console.info('Database connection has been established successfully.')
  server.listen(config.PORT, () => {
    console.info('App listening on port', config.PORT)
    app.emit('app_ready')
  })
  process.on('SIGINT', stop)
  process.on('SIGTERM', stop)
}

function stop () {
  console.log('App shutting down, waiting for remaining connections...')
  server.close(() => {
    Database.close()
    console.log('Connections closed, stopping process...')
  })
}

module.exports = {app, Database}
