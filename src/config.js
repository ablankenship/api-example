const Cryptr = require('cryptr')
const cryptr = new Cryptr('1q9x7P4xRuvm79XXUiUx1FLXS3AFlX87')

module.exports = {
  ENV: envVar('NODE_ENV') || 'development',
  PORT: parseInt(envVar('PORT')) || 8081,
  SECRET: envVar('SECRET') || 'uWwFBtHyYCMQ26QMdROrQquXNCIuE1n1',
  ROUNDS: parseInt(envVar('ROUNDS')) || 8, // Set to 12-13 in production
  DATABASE_URL: envVar('DATABASE_URL') || 'postgres://postgres:secret@127.0.0.1:5435/tradecast',
  MAILCHIMP_API_KEY: envVar('MAILCHIMP_API_KEY') || ''
}

function envVar (name) {
  if (process.env.hasOwnProperty(name)) {
    if (name === 'NODE_ENV' || name === 'PORT' || name === 'ROUNDS' || name === 'DATABASE_URL') {
      return process.env[name]
    }
    return cryptr.decrypt(process.env[name])
  }
}
