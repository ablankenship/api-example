const {Sequelize, Database, serialize, serializeMany} = require('libs/db')
const encryption = require('libs/encryption')
const serializer = require('libs/serializer')
const APIError = require('libs/errors')

const User = Database.define('user', {
  id: {
    primaryKey: true,
    type: Sequelize.UUID,
    defaultValue: Sequelize.UUIDV4
  },
  username: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      notEmpty: true
    }
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      notEmpty: true,
      isEmail: true
    }
  },
  hash: {
    type: Sequelize.STRING,
    allowNull: false
  },
  type: {
    type: Sequelize.ENUM(['basic', 'admin']),
    allowNull: false,
    defaultValue: 'basic'
  }
})

User.signup = function (body, isAdmin) {
  const self = this
  body.email = (body.email || '').toLowerCase()
  body.username = (body.username || '').toLowerCase()
  body.hash = encryption.bcrypt(body.password)

  if (!isAdmin) {
    body.type = 'basic'
  }

  return self.create(body)
}

User.updateUser = function (id, body) {
  const fields = ['email']
  const updateFields = {}
  fields.forEach((val) => {
    if (body.hasOwnProperty(val)) {
      updateFields[val] = body[val]
    }
  })
  return this.update(updateFields, {where: {id}, returning: true})
}

User.findByEmail = function (email) {
  return this.findOne({where: {email}, rejectOnEmpty: true})
}

User.prototype.authenticate = function (password, bypass) {
  const user = this
  if (bypass || encryption.bcryptCompare(password, this.hash)) {
    return {user, token: encryption.jwtSign(user)}
  }
  throw new APIError('InvalidCredentials')
}

User.prototype.updatePassword = function (body) {
  if (encryption.bcryptCompare(body.old_password, this.hash)) {
    this.set('hash', encryption.bcrypt(body.new_password))
    return this.save()
  }
  throw new APIError('InvalidCredentials')
}

User.serialize = serializeMany
User.prototype.serialize = serialize
User.serializerSchema = serializer('user')

module.exports = User
