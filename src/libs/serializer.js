const schemas = {
  user: {
    include: ['@all'],
    exclude: ['@fk', 'hash']
  }
}

module.exports = function (key) {
  return Object.assign({}, schemas[key])
}
