const uuid = require('uuid/v4')
const APIError = require('libs/errors')

exports.responses = (req, res, next) => {
  req.requestId = res.requestId = uuid()
  res.success = success
  res.failure = failure
  // console.log(req.requestId, req.method, req.path, req.headers, req.body)
  next()
}

exports.catch404 = (req, res, next) => {
  res.failure(new APIError('NotFound'))
}

exports.errors = (err, req, res, next) => {
  res.failure(err instanceof APIError ? err : new APIError(err))
}

function success (data, status, token, updated, deleted) {
  status = status || 200
  const output = {success: true, updated: updated, deleted: deleted, token: token, data}
  // console.log(this.req.requestId, output)
  this.status(status).json(output)
}

function failure (err, status) {
  // console.log(this.req.requestId, err)
  status = status || err.status
  this.status(status).json(err)
  console.log(status, err)
}