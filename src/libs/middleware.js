const {User} = require('models')
const encryption = require('libs/encryption')
const cors = require('cors')

exports.checkToken = (getUser) => {
  return (req, res, next) => {
    const token = req.headers['x-auth-token']

    if (!token) return next()

    try {
      const decoded = encryption.jwtVerify(token)
      req.authedUserId = decoded.userId
      req.authedUserName = decoded.userName
      req.authedUserType = decoded.userType
      if (!getUser) return next()

      User.findByPrimary(decoded.userId, {rejectOnEmpty: true})
        .then(user => {
          req.authedUser = user
          return next()
        })
        .catch(err => invalid(next, err))
    } catch (err) {
      return invalid(next, err)
    }
  }
}

exports.accessLevel = (l) => {
  const levels = ['basic', 'admin']
  return (req, res, next) => {
    if (req.authedUserType && levels.indexOf(req.authedUserType) >= levels.indexOf(l)) {
      return next()
    }
    next('InsufficientPrivileges')
  }
}

exports.requireToken = (getUser) => {
  return [exports.checkToken(getUser), (req, res, next) => {
    if (!req.authedUserId) {
      return invalid(next)
    }
    next()
  }]
}

exports.appStatus = (req, res, next) => {
  return res.success('ok')
}

exports.cors = cors({origin: true, credentials: true})

function invalid (next, err) {
  if (err) {
    // console.error(err)
  }
  next('InvalidAuthToken')
}
