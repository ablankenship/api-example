const config = require('config')
const Mandrill = require('mandrill-api/mandrill')
const mandrill = new Mandrill.Mandrill(config.MAILCHIMP_API_KEY)

exports.sendWelcomeEmail = (user) => {
  const message = {
    subject: 'Welcome to TradeCast!',
    from_email: 'no-reply@tradecast.co',
    from_name: 'TradeCast',
    to: [{email: user.email, name: user.full_name, type: 'to'}]
  }

  if (config.ENV !== 'testing') {
    mandrill.messages.sendTemplate({
      template_name: 'WelcomeEmail',
      template_content: [],
      message,
      async: true
    }, (result) => {
      // console.log(result)
    }, (e) => {
      console.warn('Mandrill Error', e)
    })
  }
}