const config = require('config')

class APIError {
  constructor (err, data) {
    Error.captureStackTrace(this, this.constructor)
    this.name = 'APIError'
    this.code = 'UnknownError'
    this.status = 500
    this.message = 'Unknown error occurred'
    this.success = false
    data = data || {}

    this.parseError(err, data)

    if (this.code === 'UnknownError') {
      console.error('UnknownError', err)
    }
  }

  toString () {
    let out = `${this.name} (${this.code} ${this.status}): ${this.message}`
    out = config.ENV === 'debug' ? out + `\n\n${this.stack}` : out
    return out
  }

  inspect () {
    return this.toString()
  }

  parseError (err, data) {
    let code
    if (typeof err === 'string') {
      code = err
    } else if (err instanceof Error) {
      if (err.code === 'ENOENT') {
        code = 'NotFound'
      } else {
        code = err.name
      }
    }

    if (code && ErrorOverrides.hasOwnProperty(code)) {
      const values = Object.assign({}, ErrorOverrides[code])
      const fields = values.fields || []
      if (!values.code) values.code = code
      delete values.fields
      fields.forEach((v) => {
        if (err.hasOwnProperty(v)) values[v] = err[v]
        if (data.hasOwnProperty(v)) values[v] = data[v]
      })
      Object.assign(this, values)
    }
  }
}

const ErrorOverrides = {
  // Should be handled by app

  SequelizeValidationError: {
    code: 'InputValidationError',
    status: 400,
    message: 'Invalid input data',
    fields: ['errors']
  },
  SequelizeUniqueConstraintError: {
    code: 'DuplicateEntryError',
    status: 400,
    message: 'A record already exists with this value',
    fields: ['errors']
  },
  SequelizeForeignKeyConstraintError: {
    code: 'InvalidReferenceKey',
    status: 400,
    message: 'The reference key provided does not exist'
  },

  // Safe to show
  SequelizeEmptyResultError: {
    code: 'NotFound',
    status: 404,
    message: 'The record requested does not exist'
  },
  DuplicateUserEmail: {
    code: 'DuplicateEntryError',
    status: 400,
    message: 'This username or email address is already in use'
  },
  InvalidAuthToken: {
    status: 401,
    message: 'You must be logged in to access this content'
  },
  InvalidCredentials: {
    status: 401,
    message: 'Invalid email address or password'
  },
  InvalidSubscription: {
    status: 400,
    message: 'The Apple receipt provided is invalid'
  },
  InsufficientPrivileges: {
    status: 403,
    message: 'You must have a higher access level to access this feature'
  },
  NotFound: {
    status: 404,
    message: 'NotFound'
  }
}

module.exports = APIError
