const Sequelize = require('sequelize')
const Serializer = require('sequelize-to-json')
const config = require('config')

const Database = new Sequelize(config.DATABASE_URL, {
  dialect: 'postgres',
  dialectOptions: {
    ssl: config.ENV === 'production'
  },
  typeValidation: true,
  logging: null,
  define: {
    underscoredAll: true,
    underscored: true
  }
})

function serialize (schema) {
  return (new Serializer(this.constructor, schema || this.constructor.serializerSchema)).serialize(this)
}

function serializeMany (data, schema) {
  return Serializer.serializeMany(data, this, schema || this.serializerSchema)
}

module.exports = {Database, Sequelize, serialize, serializeMany}
