const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const config = require('config')

exports.bcrypt = (password) => {
  return bcrypt.hashSync(password + config.SECRET, config.ROUNDS)
}

exports.bcryptCompare = (password, hash) => {
  return bcrypt.compareSync(password + config.SECRET, hash)
}

exports.encrypt = (str) => {
  const cipher = crypto.createCipher('aes256', config.SECRET)
  return cipher.update(str, 'utf8', 'hex') + cipher.final('hex')
}

exports.decrypt = (str) => {
  const cipher = crypto.createDecipher('aes256', config.SECRET)
  return cipher.update(str, 'hex', 'utf8') + cipher.final('utf8')
}

exports.hmac = (path, body, token) => {
  const str = path + JSON.stringify(body)
  const hmac = crypto.createHmac('sha256', config.SECRET)
  hmac.update(str)
  const hash = hmac.digest('hex')
  return (token) ? token === hash : hash
}

exports.generateBytes = (n) => {
  return crypto.randomBytes(n).toString('hex')
}

exports.jwtSign = (user, uuid) => {
  let payload = {
    uuid,
    userId: user.id,
    userName: user.username,
    userType: user.type
  }
  let options = {
    expiresIn: '24h'
  }
  return jwt.sign(payload, config.SECRET, options)
}
exports.jwtVerify = (token) => {
  return jwt.verify(token, config.SECRET)
}
