const express = require('express')
const {User} = require('models')
const router = express.Router()

router.post('/', (req, res, next) => {
  const body = req.body

  if (!body.email) {
    return next('SequelizeValidationError', {path: 'email', type: 'required'})
  }

  User.findByEmail(body.email.toLowerCase())
    .then(user => user.authenticate(body.password))
    .then(({user, token}) => res.success(user.serialize(), 200, token))
    .catch(next)
})

module.exports = router
