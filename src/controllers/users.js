const express = require('express')
const {User} = require('models')
const Sequelize = require('sequelize')
const middleware = require('libs/middleware')
const APIError = require('libs/errors')
// const mail = require('libs/mail')
const router = express.Router()

router.post('/', middleware.checkToken(), (req, res, next) => {
  const body = req.body
  const isAdmin = req.authedUserType === 'admin'

  User.signup(body, isAdmin)
    .then(user => user.authenticate(body.password))
    .then(({user, token}) => {
      res.success(user.serialize(), 200, token)
      return user
    })
    // .then(mail.sendWelcomeEmail)
    .catch(Sequelize.UniqueConstraintError, err => next('DuplicateUserEmail', err))
    .catch(next)
})

router.put('/', middleware.requireToken(), (req, res, next) => {
  const body = req.body
  User.updateUser(req.authedUserId, body)
    .then(updates => {
      if (updates.length === 2 && updates[1].length === 1) {
        return updates[1][0]
      }
      throw new APIError('UpdateFailed')
    })
    .then(user => res.success(user.serialize(), 200, undefined, true))
    .catch(next)
})

router.put('/password', middleware.requireToken(true), (req, res, next) => {
  const body = req.body

  req.authedUser.updatePassword(body)
    .then(user => res.success(user.serialize(), 200, true, true))
    .catch(next)
})
/* User.findById('87cab7a2-3856-417f-99bc-492661d3c495')
  .then(user => user.authenticate(null, true))
  .then(({user, token}) => {console.log(user, token)}) */

module.exports = router
